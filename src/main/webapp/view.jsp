<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/header.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <title>Просмотр книги</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <h1>Просмотр книги</h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Автор</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>${book.id}</td>
            <td>${book.title}</td>
            <td>${book.author}</td>
            <td>
                <a href="update?id=${book.id}" class="btn btn-primary">Редактировать</a>
                <a href="delete?id=${book.id}" class="btn btn-danger">Удалить</a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
<%@ include file="/footer.jspf"%>
